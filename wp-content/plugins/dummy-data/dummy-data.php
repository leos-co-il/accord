<?php
/**
 * Plugin Name: WordPress Dummy Data
 * Version: 1.0
 * Author: Maxim Folko@LEOS
 */


class Dummy_Data {
	protected $prefix = 'dummy_data_';


	public function __construct() {
		add_action('admin_menu', array(&$this, 'admin_register_submenu'));
		add_action("wp_ajax_{$this->prefix}ajax", array(&$this, 'insert_post_ajax'));
	}


	public function admin_register_submenu() {
		$hook = add_submenu_page('tools.php', 'נתוני דמה', 'נתוני דמה', 'manage_options', "{$this->prefix}menu.php", array(&$this, 'admin_panel'));
		add_action("admin_print_scripts-$hook", array(&$this, 'assets'));
	}


	public function assets($hook) {
		wp_register_script($this->prefix.'js', plugins_url('dummy-data.js', __FILE__), array('jquery'));
		wp_enqueue_script($this->prefix.'js');
	}

	public function admin_panel() {
		if (!current_user_can('manage_options')) {
			wp_die(__('Oops. :)'));
		}
		if (isset($_POST['_wpnonce'])) {
			if (!check_admin_referer($this->prefix."form")) { ?>
				<div class="error"><p>Your session has timed out. Please try again.</p></div><?php
			} else {
				$operation = $this->process_action(@$_POST[$this->prefix."action"]);
				if (is_wp_error($operation)) { ?>
					<div class="error"><p><strong>An error occurred: <?php echo $operation->get_error_message(); ?></strong></p></div><?php
				} else { ?>
					<div class="updated"><p><strong><?php echo (is_string($operation) ? $operation : 'Wake up Neo... The Matrix has you... Follow the white rabbit.'); ?></strong></p></div><?php
				}
			}
		}
		$categories = get_all_category_ids();
		$category_count = count($categories);
		if ($this->is_dirty()) {
			$category_count = 'זא זמין (<a href="">רענון דף</a>)';
		}


		?>		
		<div class="wrap"><div id="icon-tools" class="icon32"><br /></div>
		<h2>מייצר נתוני דמה</h2>
        <h3>פוסטים</h3>
		<form method="post" name="<?php echo $this->prefix."form"; ?>" enctype="multipart/form-data">
		<?php wp_nonce_field("{$this->prefix}form"); ?>
		<table class="form-table">
		<tr valign="top">
			<th scope="row">קטגוריות<br /><br />דוגמא:<div style="font-family: monospace; margin: 0.5em 1em;">category<br />&nbsp;child category 1<br />&nbsp;child category 2<br /></div></th>
			<td>
			<fieldset>מספר קטגוריות: <?php echo $category_count; ?><br />
			<textarea name="<?php echo $this->prefix."categories"; ?>" style="font-family: monospace; width: 100%; height: 240px;"><?php echo esc_textarea($setting['categories']); ?></textarea><br />
				<button type="submit" class="button-primary button" name="<?php echo $this->prefix."action"; ?>" value="build_category">
                    הוסף קטגוריות
                </button>
			</fieldset>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">הוספת פוסטים</th>
			<td>
			<fieldset id="<?php echo $this->prefix."ajax_insert_post"; ?>">
				<label for="<?php echo $this->prefix."num_posts"; ?>">
				כמות <input name="<?php echo $this->prefix."num_posts"; ?>" type="number" min="0" step="1" value="<?php echo esc_attr($setting['num_posts']); ?>" class="small-text" />
				</label><br />
				<label for="<?php echo $this->prefix."leaf_only"; ?>">
				<input type="checkbox" name="<?php echo $this->prefix."leaf_only"; ?>" value="1" <?php if ($setting['leaf_only'] == '1') { ?>checked="checked"<?php } ?> /> קטגוריות אב (ללא ילדים) בלבד
				</label><br />
				<label for="<?php echo $this->prefix."random_author"; ?>">
				<input type="checkbox" name="<?php echo $this->prefix."random_author"; ?>" value="1" <?php if ($setting['random_author'] == '1') { ?>checked="checked"<?php } ?> /> משתמש אקראי לכל פוסט.
				</label><br />
				<?php if ($this->is_dirty()) { ?>
				<button type="submit" class="button" name="<?php echo $this->prefix."action"; ?>" disabled="disabled" value="generate_post">Generate Posts (unavailable, please refresh page)</button>
				<?php } else { $ajax_nonce = wp_create_nonce($this->prefix.'ajax'); ?>
				<script type="text/javascript"><?php echo "var {$this->prefix}categories = [".implode(', ', $categories)."];"; ?></script>
				<script type="text/javascript"><?php echo "var {$this->prefix}ajax_nonce = '{$ajax_nonce}';"; ?></script>
				<button type="submit" class="button" name="<?php echo $this->prefix."action"; ?>" value="generate_post">הוסף פוסטים</button> <span id="<?php echo $this->prefix."ajax_response"; ?>"></span>
				<?php } ?>
			</fieldset>
			</td>
		</tr>
		</table>
		<h3>מחק פוסטים</h3>
		<table class="form-table">
		<tr valign="top">
			<th scope="row">מחק פוסטים קיימים<br />מוגבל ל 500</th>
			<td>
			<fieldset>
				<button type="submit" class="button" name="<?php echo $this->prefix."action"; ?>" value="delete_post">מחק פוסטים</button>
			</fieldset>
			</td>
		</tr>
		<tr valign="top">
			<th scope="row">מחק קטגוריות</th>
			<td>
			<fieldset>
				<button type="submit" class="button" name="<?php echo $this->prefix."action"; ?>" value="delete_category">מחק קטגוריות</button>
			</fieldset>
			</td>
		</tr>
		</table>
		</form>
		<?php
	}


	public function is_dirty() {
		$action = @$_POST[$this->prefix.'action'];
		return ($action == 'build_category' || $action == 'delete_category');
	}


	protected function process_action($action) {
		switch ($action) {
		case "build_category":
			$category_count = count(get_all_category_ids());
			if ($category_count > 1) {
				return new WP_Error('illegal', __("Please clear out the existing categories before inserting the new ones."));
			}			
			return $this->insert_category($_POST[$this->prefix . 'categories']);
		case "delete_category":
			$this->delete_category();
			return true;
		case "delete_post":
			$this->delete_post();
			return true;
		case "generate_post":
			return new WP_Error('nojavascript', __("Post generation requires JavaScript. Please make sure it is enabled or check the console for errors."));
		default:
			return true;
		}
	}

	protected function insert_category($cats) {
		$cats = explode("\n", $cats);
		$parents = array(0); // FILO Stack
		foreach ($cats as $cat) {
			$tier = $this->spaces($cat);
			if (trim($cat) == '') {
				continue;
			} else if ($tier + 1 < count($parents)) {
				array_splice($parents, $tier + 1, count($parents));
			}
			$cat_ID = wp_insert_term($this->catname($cat), 'category', array(
				'slug' => $this->slug($cat),
				'parent' => $parents[count($parents) - 1],
			));
			if (is_wp_error($cat_ID)) {
				return $cat_ID;
			} else {
				$parents[] = $cat_ID['term_id'];
			}
		}
		return true;
	}

	protected function spaces($str) {
		if (trim($str) == "") {
			return -1;
		}
		$count = 0;
		while (strlen($str) > 0 && substr($str, 0, 1) == ' ') {
			$str = substr($str, 1);
			$count++;
		}
		return $count;
	}

	protected function slug($str) {
		if (strpos($str, '//') !== false) {
			return substr($str, strpos($str, '//') + 2);
		}
		return sanitize_title($str);
	}

	protected function catname($str) {
		if (strpos($str, '//') === false) {
			return $str;
		}
		return substr($str, 0, strpos($str, '//'));
	}

	public function insert_post_ajax() {
		check_ajax_referer($this->prefix.'ajax', 'security');
		if (!current_user_can('manage_options')) {
			die('Forbidden operation.');
		}

		// Read parameters
		$n = $_POST['num_posts'];
		$leaf_only = ($_POST['leaf_only'] == '1');
		$random_author = ($_POST['random_author'] == '1');
		$categories = explode(',', $_POST['cat_id']);

		if ($random_author) {
			$authors = array_map(function($author) {
				return $author->ID;
			}, get_users(array('number' => 9999)));
		} else {
			$authors = array(get_current_user_id());
		}

		foreach ($categories as $category) {
			if (!$leaf_only || $this->is_leaf($category)) {
				for ($i = 0; $i < $n; $i++) {
					$post_title = str_replace('.', '', $this->filler_text('title'));
					$opts = array(
						'post_author' => $authors[array_rand($authors)],
						'post_content' => $this->filler_text('post'),
						'post_date' => $this->recent_date(),

						'post_status' => 'publish',
						'post_title' => $post_title,
						'post_type' => 'post',
					);
					$op = wp_insert_post($opts, true);
					if (is_wp_error($op)) {
						die($op->get_error_message());
					} else {
						wp_publish_post($op);
						wp_set_post_terms($op, array($category), 'category');
					}
				}
			}
		}

		die();
	}

	protected function is_leaf($cat_ID) {
		return (count(get_categories(array('parent' => $cat_ID, 'number' => 1, 'hide_empty' => 0))) == 0);
	}

	protected function recent_date() {
		return date('Y-m-d H:i:s', time() - rand(0, 30*24*60*60)); // Last 30 days
	}

	protected function array_random_value($a) {
		$index = array_rand($a);
		return $a[$index];
	}

	protected function filler_text($type) {

		static $lipsum_source = array(
			"לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג",
			"איפסום  <a href='/'>למטכין נשואי</a>, סאפיאן - פוסיליס קוויס",
			"סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.",
			" היושבב שערש שמחויט",
			"מנכם למטכין נשואי מנורךגולר",
			"מונפרד אדנדום סילקוף, מרגשי <a href='/'>היושבב שערש שמחויט - שלושע ותלברו </a>",
			"הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.",
			"לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג ",
			"הועניב היושבב שערש שמחויט - שלושע ותלברו ",
			"נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.",
			"מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"פוסיליס קוויס, אקווזמן קוואזי במר",
			"כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.",
			"<a href='/'>דולור סיט אמט,</a>וט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, <a href='/'>. לפמעט מוסן מנת. קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולו</a>",
			"תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס",
			"קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.",
			"לורם איפסום דולור סיט אמט",
			"קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט",
			"קלובר בריקנה סטום, לפריקך תצטריק לרטי",
			"צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה",
			"איבן איף, ברומץ כלרשט מיחוצים.",
			"<a href='/'>קלובר </a>  יש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכיותלשך וחאית נובש ע",
			"קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה.",
			"וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק",
			"גי שהכ <strong>. בראיט ולחת צורק מונחף, בגורמי מגמש. ת</strong>, תיעם גדדיש. קוויז ",
			"תתיח לרעח. לת צשחמי צש בליא",
			"סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק.",
			"השק סכעיט דז מא, מנכם למטכין נשואי מנורך. לורם איפסום דולור סיט אמט, להאמית קר",
			"סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"הועניב היושבב שערש שמחויט - שלושע ותלברו ח <strong> דולור סיט אמט, סחטיר בלובק</strong>",
			"לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאח",
			"מפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה",
			"לתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לורם איפסום דולו",
			"סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם ",
			"ושע ותלברו חשלו שעותלשך וחאית נו",
			"מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק",
			"מנורךגולר מונפרר סוברט ",
			"אן - פוסיליס קוויס, אקווזמן קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ושבעג",
			"קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים",
			"ליאמום בלינך רוגצה. לפמעט מוסן מנת. נולום ארווס סאפי",
			"מנורך. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.",
			"לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"צש בליא, מנסוטו צמלח ",
			"רר סוברט לורם שבצק יהול, לכנוץ בעריר ג",
			"נולום ארווס סאפיאן",
			"לורם איפסום דולור סיט אמט",
			"מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.",
			"דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט",
			" תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.",
			"מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.",
			"יסי טידום בעליק. קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי",
			"ברומץ כלרשט מי",
			"אאוגו וסטיבולום סוליסי טידום בעליק. קונדי",
			"לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.",
		);
		if ($type == 'title') {
			// Titles average 3 to 6 words
			$length = rand(3, 6);
			$ret = "";
			for ($i = 0; $i < $length; $i++) {
				if (!$i) {
					$ret = ucwords($this->array_random_value(explode(" ", strip_tags($this->array_random_value($lipsum_source))))) . ' ';
				} else {
					$ret .= strtolower($this->array_random_value(explode(" ", strip_tags($this->array_random_value($lipsum_source))))) . ' ';
				}
			}
			return trim($ret);
		} else if ($type == 'post') {
			$ret = "";
			$order = array('paragraph');
			$order_length = rand(12, 19);
			for ($n = 0; $n < $order_length; $n++) {
				$choice = rand(0, 8);
				switch ($choice) {
				case 0: $order[] = 'list'; break;
				case 1: $order[] = 'image'; break;
				case 2: $order[] = 'blockquote'; break;
				default: $order[] = 'paragraph'; break;
				}
			}
			for ($n = 0; $n < count($order); $n++) {
				switch ($order[$n]) {
				case 'paragraph':
					$length = rand(2,7);
					$ret .= '<p>';
					for ($i = 0; $i < $length; $i++) {
						if ($i) $ret .= ' ';
						$ret .= $this->array_random_value($lipsum_source) . '.';
					}
					$ret .= "</p>\n";
					break;
				case 'image':
					$ret .= "<p><img src='http://placehold.it/900x580' /></p>\n";
					break;
				case 'list':
					$tag = (rand(0, 1)) ? 'ul' : 'ol';
					$ret .= "<$tag>\n";
					$length = rand(2,5);
					for ($i = 0; $i < $length; $i++) {
						$ret .= "<li>" . $this->array_random_value($lipsum_source) . "</li>\n";
					}
					$ret .= "</$tag>\n";
					break;
				case 'blockquote':
					$length = rand(2,7);
					$ret .= '<blockquote><p>';
					for ($i = 0; $i < $length; $i++) {
						if ($i) $ret .= ' ';
						$ret .= $this->array_random_value($lipsum_source) . '.';
					}
					$ret .= "</p></blockquote>\n";
					break;
				}
			}
			
			return $ret;
		}
	}
	

	protected function delete_category() {
		$categories = get_all_category_ids();
		foreach ($categories as $category) {
			wp_delete_category($category);
		}
	}

	protected function delete_post() {
		$posts = get_posts(array(
			'posts_per_page' => 500,
			'offset' => 0,
		));
		foreach ($posts as $post) {
			wp_delete_post($post->ID, true);
		}
	}

}

if (is_admin()) {
	new Dummy_Data();
}
