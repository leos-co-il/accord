#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Gallery Manager\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-31 20:11+0200\n"
"PO-Revision-Date: \n"
"Last-Translator: Dennis Hoppe <Mail@DennisHoppe.de>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-KeywordsList: t;t:1,2c;__;_e;_x:1,2c;_ex:1,2c\n"
"X-Generator: Poedit 2.0.6\n"
"X-Poedit-SearchPath-0: ..\n"

#: ../classes/lightbox.php:22
msgid "Previous image"
msgstr ""

#: ../classes/lightbox.php:23
msgid "Next image"
msgstr ""

#: ../classes/lightbox.php:26
msgid "Close"
msgstr ""

#: ../classes/mocking-bird.php:17 ../classes/mocking-bird.php:20
msgid "Upgrade to Pro"
msgstr ""

#: ../classes/mocking-bird.php:19
#, php-format
msgid ""
"This feature is available in the <a href=\"%s\" target=\"_blank\">premium "
"version</a>."
msgstr ""

#: ../classes/mocking-bird.php:20
msgid "Unlock this feature"
msgstr ""

#: ../classes/mocking-bird.php:23
msgctxt "Link to the authors website"
msgid "https://dennishoppe.de/en/wordpress-plugins/gallery-manager"
msgstr ""

#: ../classes/mocking-bird.php:56
msgid ""
"If you like this free version of the lightbox, you will love "
"<u><strong>Gallery Manager Pro</strong></u>!"
msgstr ""

#: ../classes/mocking-bird.php:71
msgid "Free Version"
msgstr ""

#: ../classes/mocking-bird.php:85
msgid "Enjoy all features of the Pro Version"
msgstr ""

#: ../classes/mocking-bird.php:86
msgid ""
"If you like the free version, you will love <u><strong>Gallery Manager Pro</"
"strong></u>!"
msgstr ""

#: ../classes/options.php:23
msgid "Gallery Options"
msgstr ""

#: ../classes/options.php:24 ../classes/post-type.php:78
#: ../widgets/galleries.php:11 ../widgets/galleries.php:26
msgid "Galleries"
msgstr ""

#: ../classes/options.php:31
msgid "Settings"
msgstr ""

#: ../classes/options.php:37
msgid "Gallery Management"
msgstr ""

#: ../classes/options.php:38 ../options-page/lightbox.php:6
msgid "Lightbox"
msgstr ""

#: ../classes/options.php:39
msgid "Gallery Previews"
msgstr ""

#: ../classes/options.php:40
msgid "Taxonomies"
msgstr ""

#: ../classes/options.php:41
msgid "Gallery Archive"
msgstr ""

#: ../classes/options.php:75
msgid "Gallery Settings"
msgstr ""

#: ../classes/options.php:79
msgid "Settings saved."
msgstr ""

#: ../classes/options.php:107
msgid "Save Changes"
msgstr ""

#: ../classes/post-type.php:79
msgid "Gallery"
msgstr ""

#: ../classes/post-type.php:80
msgid "Add Gallery"
msgstr ""

#: ../classes/post-type.php:81
msgid "New Gallery"
msgstr ""

#: ../classes/post-type.php:82
msgid "Edit Gallery"
msgstr ""

#: ../classes/post-type.php:83
msgid "View Gallery"
msgstr ""

#: ../classes/post-type.php:84
msgid "Search Galleries"
msgstr ""

#: ../classes/post-type.php:85
msgid "No Galleries found"
msgstr ""

#: ../classes/post-type.php:86
msgid "No Galleries found in Trash"
msgstr ""

#: ../classes/post-type.php:87
msgid "All Galleries"
msgstr ""

#: ../classes/post-type.php:88
msgid "Gallery Index Page"
msgstr ""

#: ../classes/post-type.php:104 ../classes/taxonomies.php:40
#: ../classes/taxonomies.php:71
msgctxt "URL slug"
msgid "galleries"
msgstr ""

#: ../classes/post-type.php:124
#, php-format
msgid "Gallery updated. (<a href=\"%s\">View gallery</a>)"
msgstr ""

#: ../classes/post-type.php:125
msgid "Custom field updated."
msgstr ""

#: ../classes/post-type.php:126
msgid "Custom field deleted."
msgstr ""

#: ../classes/post-type.php:127
msgid "Gallery updated."
msgstr ""

#: ../classes/post-type.php:128
#, php-format
msgid "Gallery restored to revision from %s"
msgstr ""

#: ../classes/post-type.php:129
#, php-format
msgid "Gallery published. (<a href=\"%s\">View gallery</a>)"
msgstr ""

#: ../classes/post-type.php:130
msgid "Gallery saved."
msgstr ""

#: ../classes/post-type.php:131
msgid "Gallery submitted."
msgstr ""

#: ../classes/post-type.php:132
#, php-format
msgid "Gallery scheduled. (<a target=\"_blank\" href=\"%s\">View gallery</a>)"
msgstr ""

#: ../classes/post-type.php:133
#, php-format
msgid ""
"Gallery draft updated. (<a target=\"_blank\" href=\"%s\">Preview gallery</a>)"
msgstr ""

#: ../classes/post-type.php:145
msgid "Images"
msgstr ""

#: ../classes/post-type.php:147
msgid "Appearance"
msgstr ""

#: ../classes/post-type.php:151 ../meta-boxes/owner.php:3
msgid "Owner"
msgstr ""

#: ../classes/post-type.php:154 ../classes/post-type.php:193
msgid "Shortcode"
msgstr ""

#: ../classes/post-type.php:157
msgid "Hash"
msgstr ""

#: ../classes/scripts.php:25
msgid ""
"Do you want to remove this image from the gallery? It will not be deleted "
"from your media library."
msgstr ""

#: ../classes/taxonomies.php:14
msgid "Gallery Categories"
msgstr ""

#: ../classes/taxonomies.php:16
msgid "Categories"
msgstr ""

#: ../classes/taxonomies.php:17
msgid "Category"
msgstr ""

#: ../classes/taxonomies.php:18
msgid "All Categories"
msgstr ""

#: ../classes/taxonomies.php:19
msgid "Edit Category"
msgstr ""

#: ../classes/taxonomies.php:20
msgid "View Category"
msgstr ""

#: ../classes/taxonomies.php:21
msgid "Update Category"
msgstr ""

#: ../classes/taxonomies.php:22
msgid "Add New Category"
msgstr ""

#: ../classes/taxonomies.php:23
msgid "New Category"
msgstr ""

#: ../classes/taxonomies.php:24
msgid "Parent Category"
msgstr ""

#: ../classes/taxonomies.php:25
msgid "Parent Category:"
msgstr ""

#: ../classes/taxonomies.php:26
msgid "Search Categories"
msgstr ""

#: ../classes/taxonomies.php:27
msgid "Popular Categories"
msgstr ""

#: ../classes/taxonomies.php:28
msgid "Separate Categories with commas"
msgstr ""

#: ../classes/taxonomies.php:29
msgid "Add or remove Categories"
msgstr ""

#: ../classes/taxonomies.php:30
msgid "Choose from the most used Categories"
msgstr ""

#: ../classes/taxonomies.php:31
msgid "No Categories found."
msgstr ""

#: ../classes/taxonomies.php:40
#, php-format
msgctxt "URL slug"
msgid "%s/category"
msgstr ""

#: ../classes/taxonomies.php:45
msgid "Gallery Tags"
msgstr ""

#: ../classes/taxonomies.php:47
msgid "Tags"
msgstr ""

#: ../classes/taxonomies.php:48
msgid "Tag"
msgstr ""

#: ../classes/taxonomies.php:49
msgid "All Tags"
msgstr ""

#: ../classes/taxonomies.php:50
msgid "Edit Tag"
msgstr ""

#: ../classes/taxonomies.php:51
msgid "View Tag"
msgstr ""

#: ../classes/taxonomies.php:52
msgid "Update Tag"
msgstr ""

#: ../classes/taxonomies.php:53
msgid "Add New Tag"
msgstr ""

#: ../classes/taxonomies.php:54
msgid "New Tag"
msgstr ""

#: ../classes/taxonomies.php:55
msgid "Parent Tag"
msgstr ""

#: ../classes/taxonomies.php:56
msgid "Parent Tag:"
msgstr ""

#: ../classes/taxonomies.php:57
msgid "Search Tags"
msgstr ""

#: ../classes/taxonomies.php:58
msgid "Popular Tags"
msgstr ""

#: ../classes/taxonomies.php:59
msgid "Separate Tags with commas"
msgstr ""

#: ../classes/taxonomies.php:60
msgid "Add or remove Tags"
msgstr ""

#: ../classes/taxonomies.php:61
msgid "Choose from the most used Tags"
msgstr ""

#: ../classes/taxonomies.php:62
msgid "No Tags found."
msgstr ""

#: ../classes/taxonomies.php:71
#, php-format
msgctxt "URL slug"
msgid "%s/tag"
msgstr ""

#: ../classes/taxonomies.php:111
msgid "Archive Url"
msgstr ""

#: ../classes/taxonomies.php:114
#, php-format
msgid "This is the URL to the archive of this %s."
msgstr ""

#: ../classes/taxonomies.php:118
msgid "Archive Feed"
msgstr ""

#: ../classes/taxonomies.php:121
#, php-format
msgid "This is the URL to the feed of the archive of this %s."
msgstr ""

#: ../classes/template-tags-fallbacks.php:48
msgid "Uncategorized"
msgstr ""

#: ../classes/thumbnails.php:31
msgid "Thumbnail"
msgstr ""

#: ../classes/thumbnails.php:32
msgid "Medium"
msgstr ""

#: ../classes/thumbnails.php:33
msgid "Large"
msgstr ""

#: ../classes/thumbnails.php:34
msgid "Full Size"
msgstr ""

#: ../meta-boxes/appearance.php:5 ../options-page/previews.php:39
msgid "Columns"
msgstr ""

#: ../meta-boxes/appearance.php:15 ../options-page/previews.php:48
msgid "Size"
msgstr ""

#: ../meta-boxes/images.php:19
msgid "Remove image"
msgstr ""

#: ../meta-boxes/images.php:24
msgid "Add images"
msgstr ""

#: ../meta-boxes/owner.php:14
msgid "Changes the owner of this gallery."
msgstr ""

#: ../meta-boxes/show-code.php:3
msgid ""
"To embed this gallery in another posts content you can use this "
"<em>[gallery]</em> shortcode:"
msgstr ""

#: ../meta-boxes/show-code.php:5
msgid "Just copy this code to all places where this gallery should appear."
msgstr ""

#: ../meta-boxes/show-hash.php:3
msgid ""
"To start this gallery in a lightbox by clicking a link you can link to this "
"<em>#hash</em>:"
msgstr ""

#: ../meta-boxes/show-hash.php:5
msgid "Just use this hash as link target (href)."
msgstr ""

#: ../options-page/archive.php:6
msgid "Enable the gallery archive."
msgstr ""

#: ../options-page/archive.php:10
#, php-format
msgid ""
"The archive link for your galleries is: <a href=\"%1$s\" target=\"_blank\">"
"%1$s</a>"
msgstr ""

#: ../options-page/archive.php:14
#, php-format
msgid ""
"The archive feed for your galleries is: <a href=\"%1$s\" target=\"_blank\">"
"%1$s</a>"
msgstr ""

#: ../options-page/gallery-management.php:5
msgid "Text Editor"
msgstr ""

#: ../options-page/gallery-management.php:8
#: ../options-page/gallery-management.php:49
#: ../options-page/gallery-management.php:60 ../options-page/lightbox.php:9
#: ../options-page/lightbox.php:30 ../options-page/lightbox.php:43
#: ../options-page/lightbox.php:53 ../options-page/lightbox.php:63
msgid "On"
msgstr ""

#: ../options-page/gallery-management.php:9
#: ../options-page/gallery-management.php:19
#: ../options-page/gallery-management.php:29
#: ../options-page/gallery-management.php:39
#: ../options-page/gallery-management.php:50
#: ../options-page/gallery-management.php:61 ../options-page/lightbox.php:19
msgid "Off"
msgstr ""

#: ../options-page/gallery-management.php:11
msgid "Enables or disables text editor for galleries."
msgstr ""

#: ../options-page/gallery-management.php:16
msgid "Excerpts"
msgstr ""

#: ../options-page/gallery-management.php:21
msgid "Enables or disables text excerpts for galleries."
msgstr ""

#: ../options-page/gallery-management.php:26
msgid "Revisions"
msgstr ""

#: ../options-page/gallery-management.php:31
msgid "Enables or disables revisions for galleries."
msgstr ""

#: ../options-page/gallery-management.php:36
msgid "Comments"
msgstr ""

#: ../options-page/gallery-management.php:41
msgid "Enables or disables comments and trackbacks for galleries."
msgstr ""

#: ../options-page/gallery-management.php:46
msgid "Featured Image"
msgstr ""

#: ../options-page/gallery-management.php:52
msgid "Enables or disables the \"Featured Image\" for galleries."
msgstr ""

#: ../options-page/gallery-management.php:57
msgid "Custom Fields"
msgstr ""

#: ../options-page/gallery-management.php:63
msgid "Enables or disables the \"Custom Fields\" for galleries."
msgstr ""

#: ../options-page/lightbox.php:11
msgid "Turn this off if you do not want to use the included lightbox."
msgstr ""

#: ../options-page/lightbox.php:16
msgid "Loop mode"
msgstr ""

#: ../options-page/lightbox.php:21
msgid ""
"Enables the user to get from the last image to the first one with the \"Next "
"&raquo;\" button."
msgstr ""

#: ../options-page/lightbox.php:26
msgid "Title &amp; Description"
msgstr ""

#: ../options-page/lightbox.php:32
msgid ""
"Turn this off if you do not want to display the image title and description "
"in your lightbox."
msgstr ""

#: ../options-page/lightbox.php:34
msgid ""
"This feature won't work because the <a href=\"https://secure.php.net/manual/"
"en/book.libxml.php\" target=\"_blank\">LibXML PHP extension</a> is not "
"available on your webserver or it is too old."
msgstr ""

#: ../options-page/lightbox.php:40
msgid "Close button"
msgstr ""

#: ../options-page/lightbox.php:45
msgid ""
"Turn this off if you do not want to display a close button in your lightbox."
msgstr ""

#: ../options-page/lightbox.php:50
msgid "Indicator thumbnails"
msgstr ""

#: ../options-page/lightbox.php:55
msgid ""
"Turn this off if you do not want to display small preview thumbnails below "
"the lightbox image."
msgstr ""

#: ../options-page/lightbox.php:60
msgid "Slideshow play/pause button"
msgstr ""

#: ../options-page/lightbox.php:65
msgid ""
"Turn this off if you do not want to provide a slideshow function in the "
"lightbox."
msgstr ""

#: ../options-page/lightbox.php:70
msgid "Slideshow speed"
msgstr ""

#: ../options-page/lightbox.php:73 ../options-page/lightbox.php:90
msgid "ms"
msgstr ""

#: ../options-page/lightbox.php:74
msgid "The delay between two images in the slideshow."
msgstr ""

#: ../options-page/lightbox.php:79
msgid "Preload images"
msgstr ""

#: ../options-page/lightbox.php:82
msgid "The number of images which should be preloaded around the current one."
msgstr ""

#: ../options-page/lightbox.php:87
msgid "Animation speed"
msgstr ""

#: ../options-page/lightbox.php:91
msgid "The speed of the image change animation."
msgstr ""

#: ../options-page/lightbox.php:96
msgid "Stretch images"
msgstr ""

#: ../options-page/lightbox.php:99
msgid "No stretching"
msgstr ""

#: ../options-page/lightbox.php:100
msgid "Contain"
msgstr ""

#: ../options-page/lightbox.php:101
msgid "Cover"
msgstr ""

#: ../options-page/lightbox.php:103
msgid ""
"\"Contain\" means to scale the image to the largest size such that both its "
"width and its height can fit the screen."
msgstr ""

#: ../options-page/lightbox.php:104
msgid ""
"\"Cover\" means to scale the image to be as large as possible so that the "
"screen is completely covered by the image. Some parts of the image may be "
"cropped and invisible."
msgstr ""

#: ../options-page/lightbox.php:109
msgid "Script position"
msgstr ""

#: ../options-page/lightbox.php:112
msgid "Footer of the website"
msgstr ""

#: ../options-page/lightbox.php:113
msgid "Header of the website"
msgstr ""

#: ../options-page/lightbox.php:115
msgid ""
"Please choose the position of the javascript. \"Footer\" is recommended. Use "
"\"Header\" if you have trouble to make the lightbox work."
msgstr ""

#: ../options-page/previews.php:4
msgid "Gallery previews are randomly chosen images from a gallery."
msgstr ""

#: ../options-page/previews.php:5
msgid ""
"They will be shown where your theme would display a text excerpt for regular "
"posts usually."
msgstr ""

#: ../options-page/previews.php:28
msgid "Enable previews for auto generated excerpts."
msgstr ""

#: ../options-page/previews.php:34
msgid "Number of images"
msgstr ""

#: ../options-page/previews.php:60
msgid ""
"Enable previews for custom excerpts too. (Do not activate this option if "
"your theme displays the excerpt and the content on the same page.)"
msgstr ""

#: ../options-page/taxonomies.php:3
msgid ""
"Please select the taxonomies you want to use to classify your galleries."
msgstr ""

#: ../options-page/taxonomies.php:14 ../options-page/taxonomies.php:24
msgid "hierarchical"
msgstr ""

#: ../options-page/taxonomies.php:19
msgid "Events"
msgstr ""

#: ../options-page/taxonomies.php:19
msgid "Places"
msgstr ""

#: ../options-page/taxonomies.php:19
msgid "Dates"
msgstr ""

#: ../options-page/taxonomies.php:19
msgid "Persons"
msgstr ""

#: ../options-page/taxonomies.php:19
msgid "Photographers"
msgstr ""

#: ../widgets/galleries.php:12
msgid "Displays some of your galleries."
msgstr ""

#: ../widgets/galleries.php:44 ../widgets/random-images.php:70
#: ../widgets/taxonomies.php:49 ../widgets/taxonomy-cloud.php:49
msgid "Title:"
msgstr ""

#: ../widgets/galleries.php:46 ../widgets/random-images.php:72
#: ../widgets/taxonomies.php:51 ../widgets/taxonomy-cloud.php:51
msgid "Leave blank to use the widget default title."
msgstr ""

#: ../widgets/galleries.php:50
msgid "Number of galleries:"
msgstr ""

#: ../widgets/galleries.php:55 ../widgets/taxonomies.php:84
#: ../widgets/taxonomy-cloud.php:79
msgid "Order by:"
msgstr ""

#: ../widgets/galleries.php:57
msgid "Title"
msgstr ""

#: ../widgets/galleries.php:58
msgid "Date"
msgstr ""

#: ../widgets/galleries.php:59
msgid "Modified"
msgstr ""

#: ../widgets/galleries.php:60
msgid "Randomly"
msgstr ""

#: ../widgets/galleries.php:61
msgid "Number of comments"
msgstr ""

#: ../widgets/galleries.php:66 ../widgets/taxonomies.php:94
#: ../widgets/taxonomy-cloud.php:89
msgid "Order:"
msgstr ""

#: ../widgets/galleries.php:68 ../widgets/taxonomies.php:96
#: ../widgets/taxonomy-cloud.php:91
msgid "Ascending"
msgstr ""

#: ../widgets/galleries.php:69 ../widgets/taxonomies.php:97
#: ../widgets/taxonomy-cloud.php:92
msgid "Descending"
msgstr ""

#: ../widgets/random-images.php:11 ../widgets/random-images.php:26
msgid "Random Images"
msgstr ""

#: ../widgets/random-images.php:12
msgid "Displays some random images from your galleries."
msgstr ""

#: ../widgets/random-images.php:76
msgid "Number of images:"
msgstr ""

#: ../widgets/random-images.php:81
msgid "Columns:"
msgstr ""

#: ../widgets/random-images.php:90
msgid "Thumbnail size:"
msgstr ""

#: ../widgets/taxonomies.php:11 ../widgets/taxonomies.php:26
#: ../widgets/taxonomy-cloud.php:26
msgid "Gallery Taxonomies"
msgstr ""

#: ../widgets/taxonomies.php:12
msgid ""
"Displays your gallery taxonomies like categories, tags, events, "
"photographers, etc."
msgstr ""

#: ../widgets/taxonomies.php:55 ../widgets/taxonomy-cloud.php:55
msgid "Taxonomy:"
msgstr ""

#: ../widgets/taxonomies.php:61 ../widgets/taxonomy-cloud.php:61
msgid "Please choose the taxonomy the widget should display."
msgstr ""

#: ../widgets/taxonomies.php:65 ../widgets/taxonomy-cloud.php:65
msgid "Number of terms:"
msgstr ""

#: ../widgets/taxonomies.php:67 ../widgets/taxonomy-cloud.php:67
msgid "Leave blank to show all."
msgstr ""

#: ../widgets/taxonomies.php:80
msgid "Show gallery counts."
msgstr ""

#: ../widgets/taxonomies.php:86 ../widgets/taxonomy-cloud.php:81
msgid "Name"
msgstr ""

#: ../widgets/taxonomies.php:87 ../widgets/taxonomy-cloud.php:82
msgid "Gallery count"
msgstr ""

#: ../widgets/taxonomies.php:89 ../widgets/taxonomy-cloud.php:84
msgid "Slug"
msgstr ""

#: ../widgets/taxonomy-cloud.php:11
msgid "Gallery Taxonomy Cloud"
msgstr ""

#: ../widgets/taxonomy-cloud.php:12
msgid "Displays your gallery taxonomies as tag cloud."
msgstr ""

#: ../widgets/taxonomy-cloud.php:93
msgid "Random"
msgstr ""
