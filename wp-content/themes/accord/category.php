<?php

get_header();
$query = get_queried_object();
$posts = new WP_Query([
		'posts_per_page' => 4,
		'post_type' => 'post',
		'suppress_filters' => false,
		'tax_query' => array(
				array(
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				)
		)
]);
$published_posts = new WP_Query([
		'posts_per_page' => -1,
		'post_type' => 'post',
		'suppress_filters' => false,
		'tax_query' => array(
				array(
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $query->term_id,
				)
		)
]);
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center mb-4">
			<div class="col-auto">
				<div class="base-output text-center">
					<h1><?= $query->name; ?></h1>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && count($published_posts->posts) > 4) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-link more-link load-more-posts" data-type="post">
						<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more...'], 'he')?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($slider = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $slider,
		'img' => get_field('slider_img', $query),
	]);
}
if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $faq,
		'title' => get_field('faq_titlw', $query),
	]);
}
get_footer(); ?>
