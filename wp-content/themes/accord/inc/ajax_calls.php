<?php

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}
add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function()
{
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$type = (isset($_REQUEST['type'])) ? $_REQUEST['type'] : 'post';
	$count_cat = (isset($_REQUEST['count'])) ? $_REQUEST['count'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$id_page = (isset($_REQUEST['page'])) ? $_REQUEST['page'] : '';
	$quantity = (isset($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : '';
	$ids = explode(',', $ids_string);
	$cat = ($type === 'project') ? 'project_cat' : 'category';
	$html = '';
	$result['html'] = '';
	$result['quantity'] = '';
	if (($type === 'post') || ($type === 'project')) {
		$query = new WP_Query([
			'post_type' => $type,
			'posts_per_page' => 2,
			'post__not_in' => $ids,
			'tax_query' => $id_term ? [
				[
					'taxonomy' => $cat,
					'field' => 'term_id',
					'terms' => $id_term,
				]
			] : '',
		]);
		if ($query->have_posts()) {
			foreach ($query->posts as $item) {
				$html = load_template_part('views/partials/card', 'post', [
					'post' => $item,
				]);
				$result['html'] .= $html;
			}
			if (wp_count_posts($type)->publish >= ($quantity + 2)) {
				$result['quantity'] = true;
			}
		}
	} elseif (($type === 'company') && $id_page) {
		$query = get_field('page_companies', $id_page);
		foreach ($query as $i => $item) {
			if (!in_array($i, $ids)) {
				$html = load_template_part('views/partials/card', 'company', [
					'item' => $item,
					'num' => $i,
				]);
				$result['html'] .= $html;
			}
		}
		if (count($query) <= ($quantity + 4)) {
			$result['quantity'] = true;
		}
	}
	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
