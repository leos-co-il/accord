<?php
get_header();
$query = get_queried_object();
$services = get_posts([
    'numberposts' => -1,
    'post_type' => 'service',
	'suppress_filters' => false,
    'tax_query' => array(
        array(
            'taxonomy' => 'service_cat',
            'field' => 'term_id',
            'terms' => $query->term_id,
        )
    )
]);
$cats = get_terms([
    'taxonomy'      => 'service_cat',
    'hide_empty'    => false,
    'parent'        => 0
]);
$serv_link = opt('services_page');
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center mb-3">
					<h1><?= $query->name; ?></h1>
					<?= category_description(); ?>
				</div>
				<?php if ($cats) : ?>
					<div class="row justify-content-center align-items-stretch mb-2">
						<div class="col-lg-4 d-flex justify-content-center align-items-center cat-link-col">
							<a class="cat-link" href="<?= $serv_link ? $serv_link['url'] : ''; ?>">
								<?= lang_text(['he' => 'כל הביטוחים', 'en' => 'All insurances'], 'he'); ?>
							</a>
						</div>
						<?php foreach ($cats as $cat_item) : ?>
							<div class="col-lg-4 d-flex justify-content-center align-items-center cat-link-col">
								<a href="<?= get_term_link($cat_item); ?>" class="cat-link <?= ($cat_item->term_id === $query->term_id) ? 'active' : ''; ?>">
									<?= $cat_item->name; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="services-output-page">
		<div class="container">
			<?php if ($services) : ?>
				<div class="row align-items-stretch justify-content-center">
					<?php foreach ($services as $x => $post) : ?>
						<div class="col-xl-3 col-md-4 col-6 col-service col-service-output wow pulse" data-wow-delay="0.<?= $x * 3; ?>s">
							<?php get_template_part('views/partials/card', 'service',
								[
									'post' => $post,
								]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php if ($faq = get_field('faq_item', $query)) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $faq,
		'title' => get_field('faq_titlw', $query),
	]);
}
get_footer(); ?>
