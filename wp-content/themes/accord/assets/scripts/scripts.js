(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
	var linc2 = $('.drop-container'),
		timeoutId;
	$('.first-sub-menu').hover(function(){
		var content = $(this).children('.sub-menu').clone();
		if(content) {
			linc2.html(content);
			clearTimeout(timeoutId);
			linc2.show();
		}
	}, function(){
		timeoutId = setTimeout($.proxy(linc2,'hide'), 1000);
	});
	linc2.mouseenter(function(){
		clearTimeout(timeoutId);
	}).mouseleave(function(){
		linc2.hide();
	});
	$( document ).ready(function() {
		$('.counter-num').rCounter({
			duration: 30,
		});
		$('#service-send').click(function () {
			var urlService = $('.serv-id').data('link');
			$('#service-input').html(urlService);
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$( '.rev-pop-trigger' ).click( function() {
			var contentAll = $( this ).children('.hidden-review').html();
			$( '#reviews-pop-wrapper' ).html( contentAll );
			$( '#reviewsModal' ).modal( 'show' );
		});
		$( '#reviewsModal' ).on( 'hidden.bs.modal', function( e ) {
			$( '#reviews-pop-wrapper' ).html( '' );
		});
		$('.contact-icon-map').hover(function (){
			$(this).parent('.contact-item').children('.hidden-info-map').addClass('show');
		});
		$('.pop-trigger').click(function (passive) {
			$('.float-form').toggleClass('show-form');
		});
		$('.base-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
		});
		$('.reviews-slider').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.partners-slider').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			rtl: true,
			arrows: false,
			dots: false,
			autoplay: true,
			autoplaySpeed: 2000,
			responsive: [
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 650,
					settings: {
						slidesToShow: 2,
					}
				},
			]
		});
		$('.team-slider').slick({
			slidesToShow: 5,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1500,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$('.same-services-slider').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			rtl: true,
			arrows: true,
			dots: false,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		$( function() {
			$.fn.scrollToTop = function() {
				$( this ).click( function() {
					$( 'html, body' ).animate({scrollTop: 0}, 'slow' );
				});
			};
		});
		$( function() {
			$( '#go-top' ).scrollToTop();
		});
		var accordion = $('#accordion');
		accordion.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().addClass('active-faq');
		});
		accordion.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().removeClass('active-faq');
		});
	});
	$('.load-more-posts').click(function(e) {
		e.preventDefault();
		var btn = $(this);
		btn.addClass('loading');
		btn.append('<div class="cart-loading mx-2"><i class="fas fa-spinner fa-pulse"></i></div>');
		var termID = $(this).data('term');
		// var params = $('.take-json').html();
		var ids = '';
		var type = $(this).data('type');
		var count = $(this).data('count');
		var page = $(this).data('page');
		var quantity = $('.more-card').length;

		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				termID: termID,
				ids: ids,
				count: count,
				quantity : quantity,
				type: type,
				page: page,
				action: 'get_more_function',
			},
			success: function (data) {
				btn.removeClass('loading');
				$('.cart-loading').remove();
				if (!data.html || data.quantity) {
					btn.addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
