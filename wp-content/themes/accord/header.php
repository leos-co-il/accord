<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row justify-content-between">
				<div class="col">
				</div>
				<?php if ($logo = opt('logo')) : ?>
					<div class="col-auto header-logo-col d-flex justify-content-center align-items-center">
						<a href="/" class="logo">
							<img src="<?= $logo['url'] ?>" alt="logo">
						</a>
					</div>
				<?php endif; ?>
				<div class="col d-flex justify-content-end align-items-center p-col-0">
					<div class="langs-wrap">
						<?php site_languages(); ?>
					</div>
					<?php if ($tel = opt('tel')) : ?>
						<a href="tel:<?= $tel; ?>" class="header-tel">
							<img src="<?= ICONS ?>header-tel.png" alt="phone">
							<span class="tel-number"><?= $tel; ?></span>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="drop-container">
		</div>
		<div class="container-fluid position-relative">
			<div class="drop-menu">
				<nav class="drop-nav">
					<?php getMenu('dropdown-menu', '1', '', ''); ?>
				</nav>
			</div>
			<div class="row">
				<div class="col d-flex align-items-center">
					<button class="hamburger hamburger--spin menu-trigger" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</div>
				<div class="col-auto">
					<nav id="desctopNav" class="h-100">
						<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
					</nav>
				</div>
				<div class="col"></div>
			</div>
		</div>
	</div>
</header>
<?php
$facebook = opt('facebook');
$linkedin = opt('linkedin');
$instagram = opt('instagram');
$whatsapp = opt('whatsapp');
$link_contact = opt('contact_page');
?>
<div class="socials-line">
	<?php if ($whatsapp) : ?>
	<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link-item"
	   target="_blank">
		<i class="fab fa-whatsapp"></i>
	</a>
	<?php endif;
	if ($facebook) : ?>
		<a href="<?= $facebook; ?>" class="social-link-item">
			<i class="fab fa-facebook-f"></i>
		</a>
	<?php endif;
	if ($instagram) : ?>
		<a href="<?= $instagram; ?>" class="social-link-item">
			<i class="fab fa-instagram"></i>
		</a>
	<?php endif;
	if ($linkedin) : ?>
		<a href="<?= $linkedin; ?>" class="social-link-item">
			<i class="fab fa-linkedin-in"></i>
		</a>
	<?php endif; ?>
	<div class="social-link-item pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="popup">
	</div>
</div>
<div class="float-form">
	<div class="container">
		<div class="row justify-content-end align-items-end">
			<div class="col-sm-5 col-min-width">
				<?php get_template_part('views/partials/repeat', 'form_item', [
						'title' => opt('pop_form_title'),
						'subtitle' => opt('pop_form_subtitle'),
						'id' => '47',
				]); ?>
			</div>
		</div>
	</div>
</div>
<div class="sticky-form-block">
	<div class="container-fluid">
		<div class="row">
			<div class="<?= $link_contact ? 'col-xl-9 col-lg-8' : 'col-12'; ?> position-relative d-flex align-items-center justify-content-center">
				<div class="vwrap">
					<div class="vmove">
						<?php if ($title = opt('sticky_form_title')) : ?>
							<span class="stick-form-title vitem">
								<?= $title; ?>
							</span>
						<?php endif;
						if ($subtitle = opt('sticky_form_subtitle')) : ?>
							<span class="stick-form-subtitle vitem">
								<?= $subtitle; ?>
							</span>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php if ($link_contact) : ?>
				<div class="col-xl-3 col-lg-4">
					<a href="<?= $link_contact['url']; ?>" class="contact-link-stick">
						<?= (isset($link_contact['title']) && $link_contact['title'])
								? $link_contact['title'] : lang_text(['he' => 'כן אני מעוניין בהצעת מחיר', 'en' => 'Yes, I am interested in a quote'], 'he');
						?>
					</a>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>
