<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
?>

<footer>
	<?php if($foo_img = opt('foo_img')) : ?>
		<div class="w-100 mt-1 wow fadeInUpBig" data-wow-delay="0.2s">
			<img src="<?= $foo_img['url']; ?>" alt="buildings" class="w-100">
		</div>
	<?php endif; ?>
	<div class="footer-main">
		<a id="go-top">
			<?= svg_simple(ICONS.'to-top.svg'); ?>
		</a>
		<div class="container-fluid footer-container-menu">
			<div class="row justify-content-between align-items-start">
				<div class="col-xl-3 col-lg-auto col-sm-6 col-12 foo-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'מפת האתר', 'en' => 'Site map'], 'he'); ?>
					</h3>
					<div class="menu-border-top">
						<?php getMenu('footer-menu', '1'); ?>
					</div>
				</div>
				<div class="col-lg col-12 foo-menu foo-links-menu">
					<?php if ($foo_l_title = opt('foo_menu_title')) : ?>
						<h3 class="foo-title">
							<?php echo $foo_l_title ? $foo_l_title : lang_text(['he' => 'קישורים', 'en' => 'Hot links'], 'he'); ?>
						</h3>
					<?php endif; ?>
					<div class="menu-border-top">
						<?php getMenu('footer-links-menu', '1', 'hop-hey three-columns'); ?>
					</div>
				</div>
				<div class="col-xl-3 col-lg col-sm-6 foo-menu contacts-footer-menu">
					<h3 class="foo-title">
						<?= lang_text(['he' => 'צרו קשר', 'en' => 'Contact us'], 'he')?>
					</h3>
					<div class="menu-border-top">
						<ul class="contact-list d-flex flex-column">
							<?php if ($tel) : ?>
								<li>
									<a href="tel:<?= $tel; ?>" class="contact-info-footer">
										<?= lang_text(['he' => 'נייד:', 'en' => 'Phone:'], 'he').$tel; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($address) : ?>
								<li>
									<a href="https://waze.com/ul?q=<?= $address; ?>"
									   class="contact-info-footer" target="_blank">
										<?= lang_text(['he' => 'כתובת ראשית:', 'en' => 'Main address:'], 'he').$address; ?>
									</a>
								</li>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<li>
									<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
										<?= lang_text(['he' => 'מייל לפניות: ', 'en' => 'Email: '], 'he').$mail; ?>
									</a>
								</li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
