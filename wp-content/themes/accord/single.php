<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-xl-7 col-lg-7 col-12">
				<h1 class="post-base-title"><?php the_title(); ?></h1>
				<div class="base-output post-output">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text">
						 <?= lang_text(['he' => 'שתף', 'en' => 'Share'], 'he'); ?>
					</span>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.2s">
						<img src="<?= ICONS ?>share-facebook.png">
					</a>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.4s">
						<img src="<?= ICONS ?>share-whatsapp.png">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link wow fadeInUp" data-wow-delay="0.6s">
						<img src="<?= ICONS ?>share-mail.png">
					</a>
				</div>
			</div>
			<div class="col-xl-4 col-lg-5 col-12 page-form-col-post">
				<?php get_template_part('views/partials/repeat', 'form_item', [
					'title' => opt('post_form_title'),
					'subtitle' => opt('post_form_subtitle'),
					'id' => '42',
				]); if (has_post_thumbnail()) : ?>
					<div class="bordered-image">
						<img src="<?= postThumb(); ?>" alt="post-image">
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
		'posts_per_page' => 2,
		'post_type' => 'post',
		'post__not_in' => array($postId),
		'tax_query' => [
				[
						'taxonomy' => 'category',
						'field' => 'term_id',
						'terms' => $post_terms,
				],
		],
]);
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 2,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="same-posts mt-5">
		<?php if ($fields['same_text']) {
			get_template_part('views/partials/content', 'text_centered',
					[
							'text' => $fields['same_text'],
					]);
			} ?>
		<div class="container">
			<?php if (!$fields['same_text']) : ?>
				<div class="row">
					<div class="col-12">
						<h2 class="base-title">
							<?= lang_text(['he' => 'ללמאמרים נוספים בתחום', 'en' => 'More articles you may interested in'], 'he'); ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $i => $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
