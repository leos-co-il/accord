<?php

the_post();
get_header();
$fields = get_fields();
$post_link = get_the_permalink();
?>

<article class="page-body p-block mb-5">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-between">
			<div class="col-12 d-flex align-items-start flex-sm-nowrap flex-wrap">
				<div class="service-page-icon">
					<?php if ($fields['serv_icon']) : ?>
						<img src="<?= $fields['serv_icon']['url']; ?>" alt="service-icon">
					<?php endif; ?>
				</div>
				<div class="flex-grow-1">
					<h1 class="post-base-title"><?php the_title(); ?></h1>
					<div class="base-output">
						<?= $fields['serv_description']; ?>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="base-output my-5">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['service_content']) {
		get_template_part('views/partials/content', 'block_content',
				[
						'content' => $fields['service_content'],
						'img' => $fields['service_image'] ? $fields['service_image']['url'] : (has_post_thumbnail() ? postThumb() : ''),
				]);
	}?>
	<div class="container">
		<div class="row justify-content-center form-wrapper">
			<div class="col-12">
				<div class="service-form-block">
					<div class="serv-id" data-link="<?= $post_link; ?>"></div>
					<?php if ($title = opt('base_form_tile')) : ?>
						<h2 class="form-title"><?= $title; ?></h2>
					<?php endif;
					getForm('43'); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php
$postId = get_the_ID();
$post_terms = wp_get_object_terms($postId, 'service_cat', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 5,
	'post_type' => 'service',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'service_cat',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($fields['same_services']) {
	$samePosts = $fields['same_services'];
} elseif ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 5,
		'orderby' => 'rand',
		'post_type' => 'post',
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="same-posts same-services mt-5">
		<?php if ($fields['same_text']) {
			get_template_part('views/partials/content', 'text_centered',
				[
					'text' => $fields['same_text'],
				]);
		} ?>
		<div class="container">
			<?php if (!$fields['same_text']) : ?>
				<div class="row">
					<div class="col-12">
						<h2 class="base-title">
							<?= lang_text(['he' => 'ללמאמרים נוספים בתחום', 'en' => 'More articles you may interested in'], 'he'); ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch arrows-slider arrows-slider-base">
				<div class="col-12">
					<div class="same-services-slider" dir="rtl">
						<?php foreach ($samePosts as $i => $post) : ?>
							<div>
								<?php get_template_part('views/partials/card', 'service', [
										'post' => $post,
									]); ?>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['form_item']) {
	get_template_part('views/partials/content', 'form_tabs', [
		'item' => $fields['form_item'],
		'text' => $fields['forms_block_text'],
	]);
}
if (isset($fields['single_slider_seo']) && $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if (isset($fields['faq_item']) && $fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
		'faq' => $fields['faq_item'],
		'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
