<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();
if ($fields['h_main_text'] || $fields['h_main_img'] || $fields['h_main_links']) : ?>
	<section class="main-home">
		<div class="container">
			<?php if ($fields['h_main_text']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="base-output text-center mb-30">
							<?= $fields['h_main_text']; ?>
						</div>
					</div>
				</div>
			<?php endif;
			if ($fields['h_main_links']) : ?>
				<div class="row justify-content-center align-items-end">
					<?php foreach ($fields['h_main_links'] as $link) : ?>
						<div class="col-auto">
							<div class="d-flex flex-column justify-content-start">
								<div class="main-icon-wrap">
									<?php if ($link['icon']) : ?>
										<img src="<?= $link['icon']['url']; ?>" alt="service-type">
									<?php endif; ?>
								</div>
								<?php if ($link['link']) : ?>
									<a href="<?= $link['link']['url']; ?>" class="main-link-tab">
										<?= isset($link['link']['title']) && $link['link']['title'] ? $link['link']['title'] : ''; ?>
									</a>
								<?php endif; ?>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
		<?php if ($fields['h_main_img']) : ?>
			<div class="main-image wow fadeInUpBig" data-wow-delay="0.2s" data-wow-duration="1.2s">
				<img src="<?= $fields['h_main_img']['url']; ?>" alt="buildings">
			</div>
		<?php endif; ?>
	</section>
<?php endif; ?>
<section class="repeat-form-block m-0">
	<div class="container">
		<div class="row justify-content-center form-wrapper">
			<div class="col-12">
				<?php if ($fields['h_main_form_title']) : ?>
					<h2 class="form-title"><?= $fields['h_main_form_title']; ?></h2>
				<?php endif;
				getForm('46'); ?>
			</div>
		</div>
	</div>
</section>
<?php if ($fields['h_about_text']) : ?>
	<section class="home-about-block">
		<div class="container">
			<div class="row justify-content-lg-between justify-content-center align-items-center">
				<?php if ($fields['h_about_img']) : ?>
					<div class="col-lg-4">
						<div class="bordered-image">
							<img src="<?= $fields['h_about_img']['url']; ?>" alt="about-us">
						</div>
					</div>
				<?php endif; ?>
				<div class="col-xl-7 col-lg-8 col-12 d-flex flex-column">
					<div class="base-output">
						<?= $fields['h_about_text']; ?>
					</div>
					<?php if (['h_about_link']) : ?>
						<a href="<?= $fields['h_about_link']['url'];?>" class="base-link align-self-end">
							<?= (isset($fields['h_about_link']['title']) && $fields['h_about_link']['title'])
									? $fields['h_about_link']['title'] : lang_text(['he' => 'קרא עוד עלינו', 'en' => 'Read more about us'], 'he');
							?>
						</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'counter');
if ($fields['h_services_text'] || $fields['h_services']) : ?>
	<section class="services-home">
		<?php if ($fields['h_services']) : $services = array_chunk($fields['h_services'], 8); ?>
			<div class="container arrows-slider arrows-slider-black">
				<div class="row justify-content-center">
					<div class="col-12">
						<div class="base-slider" dir="rtl">
							<?php foreach ($services as $service_slide) : ?>
								<div>
									<div class="serv-slide-wrapper">
										<?php foreach ($service_slide as $service) {
											echo '<div class="col-md-3 col col-service">';
											get_template_part('views/partials/card', 'service', [
													'post' => $service,
											]);
											echo '</div>';
										} ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'form');
get_template_part('views/partials/repeat', 'clients');
if ($fields['review_item']) : ?>
	<section class="reviews-block">
		<div class="container">
			<div class="row justify-content-center arrows-slider arrows-slider-black">
				<div class="col-12">
					<div class="reviews-slider" dir="rtl">
						<?php $logo = opt('logo');
						foreach ($fields['review_item'] as $x => $review) : ?>
							<div class="review-slide">
								<div class="review-item">
									<div class="quote quote-top">
										<img src="<?= ICONS ?>quote-top.png" alt="quotes">
									</div>
									<div class="quote quote-bottom">
										<img src="<?= ICONS ?>quote-bottom.png" alt="quotes">
									</div>
									<div class="rev-pop-trigger">
										+
										<div class="hidden-review">
											<h3 class="base-item-title review-title"><?= $review['rev_name']; ?></h3>
											<div class="base-output">
												<?= $review['rev_text']; ?>
											</div>
										</div>
									</div>
									<div class="rev-content">
										<div class="d-flex justify-content-center align-items-center mb-3">
											<?php if ($review['rev_img']) : ?>
												<img src="<?= $review['rev_img']['url']; ?>" alt="logo-review">
											<?php elseif ($logo) : ?>
												<img src="<?= $logo['url']; ?>" alt="logo-accord">
											<?php endif; ?>
										</div>
										<h3 class="review-title"><?= $review['rev_name']; ?></h3>
										<div class="base-text text-center">
											<?= text_preview($review['rev_text'], '30'); ?>
										</div>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
	<div class="modal fade" id="reviewsModal" tabindex="-1" role="dialog"
		 aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
			<div class="modal-content">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
				<div class="modal-body" id="reviews-pop-wrapper"></div>
			</div>
		</div>
	</div>
<?php endif;
if ($fields['h_posts_text'] || $fields['h_posts']) : ?>
	<section class="home-posts-block">
		<?php get_template_part('views/partials/content', 'text_centered',
				[
						'text' => $fields['h_posts_text'],
				]); ?>
		<div class="container">
			<?php if ($fields['h_posts']) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($fields['h_posts'] as $post) {
						get_template_part('views/partials/card', 'post',
								[
										'post' => $post,
								]);
					} ?>
				</div>
			<?php endif;
			if (['h_posts_link']) : ?>
				<div class="row justify-content-end">
					<div class="col-auto">
						<a href="<?= $fields['h_posts_link']['url'];?>" class="base-link">
							<?= (isset($fields['h_posts_link']['title']) && $fields['h_posts_link']['title'])
									? $fields['h_posts_link']['title'] : lang_text(['he' => 'לכל המאמרים', 'en' => 'To all articles'], 'he');
							?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
