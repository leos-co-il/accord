<?php
/*
Template Name: אודות
*/

get_header();
$fields = get_fields();
?>

<article class="page-body about-page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<h1 class="about-base-title text-center"><?php the_title(); ?></h1>
				<div class="base-output text-center">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part('views/partials/repeat', 'counter');
	if ($fields['about_content_text']) {
		get_template_part('views/partials/content', 'block_content',
				[
						'content' => $fields['about_content_text'],
						'img' => $fields['about_content_img'] ? $fields['about_content_img']['url'] : '',
				]);
	} ?>
</article>
<?php get_template_part('views/partials/repeat', 'benefits');
get_template_part('views/partials/repeat', 'form');
if ($fields['member_item'] || $fields['about_team_text']) : ?>
	<section class="about-team-block">
		<?php get_template_part('views/partials/content', 'text_centered',
				[
						'text' => $fields['about_team_text'],
				]);
		if ($fields['member_item']) : ?>
			<div class="container arrows-slider arrows-slider-black">
				<div class="row">
					<div class="col-12">
						<div class="team-slider" dir="rtl">
							<?php foreach ($fields['member_item'] as $member) : ?>
								<div class="slide-team-padding">
									<div class="member-item">
										<div class="bordered-image">
											<div class="member-image" <?php if ($member['member_img']) : ?>
												style="background-image: url('<?= $member['member_img']['url']; ?>')"
											<?php endif; ?>></div>
										</div>
										<h3 class="member-title font-weight-bold">
											<?= lang_text(['he' => 'שם איש צוות: ', 'en' => "Worker's name: "], 'he').$member['member_name']; ?>
										</h3>
										<h3 class="member-title">
											<?= lang_text(['he' => 'תפקיד: ', 'en' => 'Position: '], 'he').$member['member_position']; ?>
										</h3>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'clients');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
			'content' => $fields['single_slider_seo'],
			'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
