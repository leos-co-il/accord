<?php
/*
Template Name: צור קשר
*/

get_header();
$fields = get_fields();

$tel = opt('tel');
$tel_2 = $fields['tel_2'];
$mail = opt('mail');
$address = opt('address');
$fax = opt('fax');
$open_hours = opt('open_hours');
$map = opt('map');
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center mb-5">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<div class="col-12">
				<div class="row justify-content-center">
					<?php if ($tel || $tel_2) : ?>
						<div class="col-xl col-lg-3 col-sm-6 col-11 contact-item contact-item-link wow fadeInDown" data-wow-delay="0.2s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-tel.png">
							</div>
							<h4 class="contact-item-title">
								<?= lang_text(['he' => 'טלפון', 'en' => 'Phone'], 'he'); ?>
							</h4>
							<a href="tel:<?= $tel ? $tel : $tel_2; ?>" class="contact-info">
								<?= $tel.'</br>'.$tel_2; ?>
							</a>
						</div>
					<?php endif;
					if ($mail) : ?>
						<div class="contact-item col-xl col-lg-3 col-sm-6 col-11 contact-item-link wow fadeInDown"
							 data-wow-delay="0.4s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-mail.png">
							</div>
							<h4 class="contact-item-title">
								<?= lang_text(['he' => 'מייל', 'en' => 'Email'], 'he'); ?>
							</h4>
							<a href="mailto:<?= $mail; ?>" class="contact-info">
								<?= $mail; ?>
							</a>
						</div>
					<?php endif;
					if ($fax) : ?>
						<div class="contact-item col-xl col-lg-3 col-sm-6 col-11 wow fadeInDown" data-wow-delay="0.6s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-fax.png">
							</div>
							<h4 class="contact-item-title">
								<?= lang_text(['he' => 'פקס', 'en' => 'Fax'], 'he'); ?>
							</h4>
							<div class="contact-info">
								<?= $fax; ?>
							</div>
						</div>
					<?php endif; ?>
					<?php if ($address) : ?>
						<div class="contact-item-link col-xl contact-item col-lg-3 col-sm-6 col-11 wow fadeInDown" data-wow-delay="0.8s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-geo.png">
							</div>
							<h4 class="contact-item-title">
								<?= lang_text(['he' => 'כתובת', 'en' => 'Address'], 'he'); ?>
							</h4>
							<a class="contact-info" href="https://www.waze.com/ul?q=<?= $address; ?>">
								<?= $address; ?>
							</a>
						</div>
					<?php endif;
					if ($open_hours) : ?>
						<div class="contact-item-link col-xl contact-item col-lg-3 col-sm-6 col-11 wow fadeInDown" data-wow-delay="1s">
							<div class="contact-icon-wrap">
								<img src="<?= ICONS ?>contact-hours.png">
							</div>
							<h4 class="contact-item-title">
								<?= lang_text(['he' => 'שעות פעילות', 'en' => 'Activity time'], 'he'); ?>
							</h4>
							<p class="contact-info">
								<?= $open_hours; ?>
							</p>
						</div>
					<?php endif; ?>
				</div>
			</div>
			<div class="col-12">
				<div class="contact-form-back">
					<?php if ($fields['contact_form_title'] || $fields['contact_form_subtitle']) : ?>
						<h2 class="form-title mb-3">
							<?= $fields['contact_form_title']; ?>
						</h2>
					<?php endif;
					getForm('41'); ?>
				</div>
			</div>
			<?php if ($map) : ?>
				<div class="col-12 mb-3 map-col">
					<?= $map; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_footer('contact'); ?>
