<?php
/*
Template Name: מאמרים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'suppress_filters' => false
]);
$published_posts = '';
$count_posts = wp_count_posts();
if ( $count_posts ) {
	$published_posts = $count_posts->publish;
}
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center mb-4">
			<div class="col-auto">
				<div class="base-output text-center">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts && $published_posts > 4) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="base-link more-link load-more-posts" data-type="post">
						<?= lang_text(['he' => 'טען עוד..', 'en' => 'Load more...'], 'he')?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'form');
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
