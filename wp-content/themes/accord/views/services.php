<?php
/*
Template Name: שירותים
*/

get_header();
$fields = get_fields();
$posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'service',
	'suppress_filters' => false
]);
$cats = get_terms([
		'taxonomy' => 'service_cat',
		'hide_empty' => false,
]);
?>

<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center mb-3">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
				<?php if ($cats) : ?>
					<div class="row justify-content-center align-items-stretch mb-2">
						<div class="col-lg-4 d-flex justify-content-center align-items-center cat-link-col">
							<div class="cat-link active">
								<?= lang_text(['he' => 'כל הביטוחים', 'en' => 'All insurances'], 'he'); ?>
							</div>
						</div>
						<?php foreach ($cats as $cat_item) : ?>
							<div class="col-lg-4 d-flex justify-content-center align-items-center cat-link-col">
								<a href="<?= get_term_link($cat_item); ?>" class="cat-link">
									<?= $cat_item->name; ?>
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="services-output-page">
		<div class="container">
			<?php if ($posts->have_posts()) : ?>
				<div class="row align-items-stretch justify-content-center">
					<?php foreach ($posts->posts as $x => $post) : ?>
						<div class="col-xl-3 col-md-4 col-6 col-service col-service-output wow pulse" data-wow-delay="0.<?= $x * 3; ?>s">
							<?php get_template_part('views/partials/card', 'service',
								[
										'post' => $post,
								]); ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
