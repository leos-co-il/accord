<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-6 col-12 post-col post-col-base">
		<div class="post-item more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-item-image" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')"
				<?php endif;?>>
			</a>
			<div class="post-item-content-wrap">
				<div class="item-content-inside">
					<a class="post-item-title" href="<?= $link; ?>"><?= $args['post']->post_title; ?></a>
					<p class="base-text">
						<?= text_preview($args['post']->post_content, 20); ?>
					</p>
				</div>
				<a href="<?= $link; ?>" class="base-link post-link align-self-end">
					<?= lang_text(['he' => 'המשך קריאה', 'en' => 'Continue reading'], 'he'); ?>
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
