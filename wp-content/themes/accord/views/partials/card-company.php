<?php if (isset($args['post']) && $args['post']) : ?>
	<a class="service-card" href="<?= $args['post']['company_url']; ?>">
		<div class="service-icon-wrap company-icon-wrap">
			<?php if ($args['post']['company_logo']) : ?>
				<img src="<?= $args['post']['company_logo']['url']; ?>" alt="service-icon">
			<?php endif; ?>
		</div>
		<h4 class="service-title"><?= $args['post']['company_name']; ?></h4>
	</a>
<?php endif; ?>
