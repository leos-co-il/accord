<?php if (isset($args['content']) && ($args['content'])) :
	$slider_img = isset($args['img']) ? ($args['img']) : ''; ?>
	<div class="base-slider-block arrows-slider arrows-slider-base">
		<div class="container-fluid">
			<div class="row justify-content-end align-items-center position-relative">
				<?php if ($slider_img) : ?>
					<div class="col-12 slider-img-col-mob">
						<img src="<?= $slider_img['url']; ?>" alt="slider-image">
					</div>
				<?php endif; ?>
				<?php if ($slider_img) : ?>
					<div class="col-xl-7 col-lg-10 col-12 slider-img-col" style="background-image: url('<?= $slider_img['url']; ?>')">
					</div>
				<?php endif; ?>
				<div class="<?= $slider_img ? 'col-xl-6 col-lg-8 col-12' : 'col-12'; ?> slider-col-content">
					<div class="slider-wrapper-text">
						<div class="base-slider" dir="rtl">
							<?php foreach ($args['content'] as $content) : ?>
								<div class="slider-base-item">
									<div class="base-output slider-output">
										<?= $content['content']; ?>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
