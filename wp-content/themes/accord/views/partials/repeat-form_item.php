<?php
$f_title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('pop_form_title');
$f_subtitle = (isset($args['subtitle']) && $args['subtitle']) ? $args['subtitle'] : opt('pop_form_subtitle');
$id = (isset($args['id']) && $args['id']) ? $args['id'] : '47';
?>
<div class="post-form-wrap">
	<?php if ($f_title) : ?>
		<h2 class="post-form-title"><?= $f_title; ?></h2>
	<?php endif;
	if ($f_subtitle) : ?>
		<h3 class="post-form-subtitle"><?= $f_subtitle; ?></h3>
	<?php endif; ?>
	<div class="padding-form-item">
		<?php getForm($id); ?>
	</div>
</div>
