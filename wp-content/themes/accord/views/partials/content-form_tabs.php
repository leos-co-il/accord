<?php if(isset($args['item']) && $args['item']) : ?>
	<section class="form-tabs-block">
		<div class="container">
			<div class="row align-items-stretch">
				<div class="col-lg-4 col-12 mb-lg-0 mb-4">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item col nav-item-title">
							<span class="form-tab-title">
								<?= lang_text(['he' => 'טפסים להורדה', 'en' => 'Download forms'], 'he'); ?>
							</span>
							<img src="<?= ICONS ?>arrow-white-left.svg" alt="arrow">
						</li>
						<?php foreach ($args['item'] as $num => $item) : ?>
							<li class="nav-item col">
								<a class="nav-link <?= ( $num === 0) ? 'active' : ''; ?>" id="<?= $num; ?>-tab" data-toggle="tab" href="#faq-<?= $num; ?>" role="tab"
								   aria-controls="<?= $num; ?>" aria-selected="true">
									<?php if ($item['form_tab_title']) : ?>
										<h4 class="form-tab-title"><?= $item['form_tab_title']; ?></h4>
									<?php endif; ?>
								</a>
							</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<div class="col-lg-8 col-12">
					<div class="tab-content">
						<?php foreach ($args['item'] as $num => $content) : ?>
							<div class="tab-pane fade <?= ( $num === 0) ? 'show active' : ''; ?>" id="faq-<?= $num; ?>" role="tabpanel" aria-labelledby="<?= $num; ?>-tab">
								<div class="tab-content-inside">
									<?php if ($content['form_tab_content']) : foreach ($content['form_tab_content'] as $file_item) : ?>
										<div class="file-item">
											<?php if ($file_item['file_url']) {
												echo svg_simple(ICONS.'pdf.svg');
											} ?>
											<div class="file-item-content">
												<h3 class="file-item-title">
													<?= $file_item['file_title']; ?>
												</h3>
												<p class="base-text">
													<?= $file_item['file_text']; ?>
												</p>
											</div>
											<?php if ($file_item['file_url']) : ?>
												<a href="<?= $file_item['file_url']['url']; ?>" class="d-flex justify-content-center align-items-center align-self-center">
													<img src="<?= ICONS ?>download.png" alt="download" class="image-down">
												</a>
											<?php endif; ?>
										</div>
									<?php endforeach; endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
