<?php if (isset($args['content']) && $args['content']) : ?>
	<div class="block-more-content my-4">
		<div class="container">
			<div class="row align-items-stretch justify-content-center">
				<?php if (isset($args['img']) && $args['img']) : ?>
					<div class="col-lg-4 mb-lg-0 mb-4">
						<div class="bordered-image">
							<img src="<?= $args['img']; ?>" alt="image">
						</div>
					</div>
				<?php endif; ?>
				<div class="col-lg col-12">
					<div class="content-wrapper">
						<div class="base-output">
							<?= $args['content']; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
