<?php if ($counter = opt('counter_item')) : ?>
	<section class="counter-block" id="start-count">
		<div class="counter-wrapper blue-back">
			<div class="container">
				<div class="row justify-content-center align-items-start">
					<?php foreach ($counter as $counter_item) : ?>
						<div class="col-counter col-4 mb-4 wow fadeInUp">
							<div class="counter-item">
								<div class="counter-num-wrap">
									<h2 class="counter-number counter-num" data-from="0"
										data-to="<?= $counter_item['counter_number']; ?>" data-speed="1500">
										<?= $counter_item['counter_number']; ?>
									</h2>
								</div>
								<h4 class="counter-title"><?= $counter_item['counter_title']; ?></h4>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif; ?>
