<?php
$partners = opt('partner_item');
$text = opt('partners_text');
if ($partners) : ?>
	<div class="clients-block">
		<?php if ($text) {
			get_template_part('views/partials/content', 'text_centered',
					[
							'text' => $text,
					]);
		} ?>
		<div class="container">
			<div class="row justify-content-center align-items-stretch partners-container">
				<?php foreach ($partners as $n => $partner_item) : if ($partner_item['partner_logo']) : ?>
					<div class="col-xl-2 col-lg-3 col-sm-4 col-6 mb-4 partner-item wow zoomIn"
						 data-wow-delay="0.<?= $n + 2; ?>s">
						<a href="<?= isset($partner_item['partner_link']) ? $partner_item['partner_link']['url'] : ''; ?>" class="client-item">
							<img src="<?= $partner_item['partner_logo']['url']; ?>" alt="clients-logo">
						</a>
					</div>
				<?php endif; endforeach; ?>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="partners-slider" dir="rtl">
						<?php foreach ($partners as $n => $partner_item) : if ($partner_item['partner_logo']) : ?>
							<div>
								<a href="<?= isset($partner_item['partner_link']) ? $partner_item['partner_link']['url'] : ''; ?>" class="client-item">
									<img src="<?= $partner_item['partner_logo']['url']; ?>" alt="clients-logo">
								</a>
							</div>
						<?php endif; endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
