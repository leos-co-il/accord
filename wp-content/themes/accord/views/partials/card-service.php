<?php if (isset($args['post']) && $args['post']) : $icon = get_field('serv_icon', $args['post']->ID); ?>
	<a class="service-card" href="<?= get_the_permalink($args['post']); ?>">
		<div class="service-icon-wrap">
			<?php if ($icon) : ?>
				<img src="<?= $icon['url']; ?>" alt="service-icon">
			<?php endif; ?>
		</div>
		<h4 class="service-title"><?= $args['post']->post_title; ?></h4>
	</a>
<?php endif; ?>
