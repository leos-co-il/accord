<?php if (isset($args['text'])) : ?>
	<div class="container">
		<div class="row">
			<div class="col mb-30">
				<div class="base-output text-center">
					<?= $args['text']; ?>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
