<?php
$benefits = opt('benefit_item');
$ben_text = opt('benefits_text');
if ($benefits || $ben_text) : ?>
	<div class="benefits-block">
		<?php if ($ben_text) {
			get_template_part('views/partials/content', 'text_centered',
					[
							'text' => $ben_text,
					]);
		} ?>
		<div class="container-fluid">
			<?php if ($benefits) : ?>
				<div class="row justify-content-center align-items-stretch">
					<?php foreach ($benefits as $x => $why_item) : ?>
						<div class="col-xl-4 col-lg-6 col-sm-10 col-12 why-item-col wow zoomIn" data-wow-delay="0.<?= $x + 1; ?>s">
							<div class="why-item">
								<div class="why-icon-wrap">
									<?php if ($why_item['ben_icon']) : ?>
										<img src="<?= $why_item['ben_icon']['url']; ?>">
									<?php endif; ?>
								</div>
								<div class="why-content-wrap">
									<h3 class="why-item-title">
										<?= $why_item['ben_title']; ?>
									</h3>
									<p class="base-text">
										<?= $why_item['ben_text']; ?>
									</p>
								</div>
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="benefits-image wow slideInUp" data-wow-delay="0.2s">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<img src="<?= IMG ?>benefits-img.png" alt="buildings" class="w-100">
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
