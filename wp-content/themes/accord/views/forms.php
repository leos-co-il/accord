<?php
/*
Template Name: טפסים
*/

get_header();
$fields = get_fields();
?>
<article class="page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-8 col-lg-10 col-12">
				<div class="base-output text-center mb-3">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($fields['company_item']) : ?>
		<div class="container">
			<div class="row align-items-stretch justify-content-center">
				<?php foreach ($fields['company_item'] as $x => $post) : ?>
					<div class="col-xl-3 col-md-4 col-6 col-service col-service-output wow pulse" data-wow-delay="0.<?= $x * 3; ?>s">
						<?php get_template_part('views/partials/card', 'company',
								[
										'post' => $post,
								]); ?>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php if ($fields['form_item']) {
	get_template_part('views/partials/content', 'form_tabs', [
			'item' => $fields['form_item'],
			'text' => $fields['forms_block_text'],
	]);
}
if ($fields['faq_item']) {
	get_template_part('views/partials/content', 'faq', [
			'faq' => $fields['faq_item'],
			'title' => $fields['faq_titlw'],
	]);
}
get_footer(); ?>
